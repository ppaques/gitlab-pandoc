# gitlab markdown to pdf docker
This docker allow you to fully convert `gitlab markdown` files to a pdf.

The docker supports:
   - up to date pandoc (as the date of today)
   - **mermaid support**
   - **plantuml support**

## Usage

Easy tests:
 - **TODO**



## Example for final usage in my company
I use it the following way for team documentation:
  - Have a repository with markdown
  - each developper/reviewer can push to it and leverage the merge request system to track changes
  - A pipeline is launched at each master
  - A file is ready with a tag name when I need an aglomerated version.


See the **TODO** folder for a sample of a big document structure using.


## Credits
highly inspired by [pandoc-mermaid-docker](https://gitlab.com/VincentTam/pandoc-mermaid-docker). I even started by forking it then modify it all.


This image uses [Ubuntu Mono][ubuntu-fonts] and [Noto Emoji][noto-emoji] fonts
respectively license under [Ubuntu Font License][ubu-lic] and
[SIL Open Font License v1.1][noto-lic].

[ubuntu-fonts]: https://design.ubuntu.com/font/
[noto-emoji]: https://github.com/googlefonts/noto-emoji
[ubu-lic]: https://ubuntu.com/legal/font-licence
[noto-lic]: https://github.com/googlefonts/noto-emoji/blob/master/fonts/LICENSE
