FROM pandoc/latex:3.1
# Install necessary packages
#  git: used in my script for describe version of documents
#  py3-pip: mermaid dependency
#  npm: mermaid package
#  chromium: mermaid chrome for puppeteer
#  plantuml: for plantuml...
#  graphviz: because I like it
RUN apk add --update --no-cache \
    git \
    py3-pip \
    npm \
    chromium \
    plantuml \
    graphviz

RUN tlmgr install \
            koma-script \
            adjustbox \
            footnotebackref \
            pagecolor \
            hardwrap \
            catchfile \
            mdframed \
            zref \
            needspace \
            sourcesanspro \
            sourcecodepro \
            ly1 \
            titling \
            sectsty &&\
            rm -rf  /opt/texlive/texdir/texmf-dist/doc  \
                    /opt/texlive/texdir/readme-html.dir \
                    /opt/texlive/texdir/readme-txt.dir  \
                    /opt/texlive/texdir/install-tl*



# plantuml
RUN pip --no-cache-dir install pandoc-plantuml-filter pandoc-include


# install mermaid
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true \
    PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium-browser \
    PUPPETEER_CFG=/etc/default/puppeteer.json \
    MERMAID_CFG=/etc/default/mermaid.json

ADD puppeteer.json /etc/default/puppeteer.json
ADD mermaid.json /etc/default/mermaid.json

# Puppeteer v13.5.0 works with Chromium 100.
RUN npm install -g puppeteer && \
    npm install -g @mermaid-js/mermaid-cli && \
    npm cache clean --force
RUN pip --no-cache-dir install git+https://github.com/PPaques/pandoc-mermaid-filter


# Install Noto Color Emoji
RUN mkdir -p /usr/share/fonts/truetype/noto \
    && cd /usr/share/fonts/truetype/noto \
    && wget https://raw.githubusercontent.com/googlefonts/noto-emoji/master/fonts/NotoColorEmoji.ttf \
    && fc-cache -fv

# Fix wanring when building
ENV OSFONTDIR=/usr/share/fonts
RUN chmod -R o+w /opt/texlive/texdir/texmf-var \
  && fc-cache --really-force --verbose

# RUN apk --no-cache add build-base linux-headers python3-dev && \
#     pip install --no-cache-dir --force-reinstall git+https://github.com/tomduck/pandoc-xnos@284474574f51888be75603e7d1df667a0890504d#egg=pandoc-xnos && \
#     apk del build-base linux-headers python3-dev


ENTRYPOINT []
CMD ["/bin/sh"]
